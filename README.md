# CloudFormation Templates:
___

## BaseVPC.template
  
Builds a multi-subnet VPC spanning 2 Availabity Zones (AZs). Including:

### 8 Subnets in 2 AZ's (4 Subnets per AZ)  

1 Public facing subnet (Public) in AZ1  
1 Public facing subnet (Public) in AZ2  
3 private subnets (Application/Database/Management)	in each AZ1  
3 private subnets (Application/Database/Management)	in each AZ2  

Instances should be spun up in the zone that is most representative of their function. Traffic should be restricted between zones (for example Public 
-> Application, Application -> Data).   
  
The management subnets should not allow inbound traffic from the other zones, it should originate from the management zone.  

### 3 Route Tables
  
1 Public route table associated with both Public subnets. The default route takes traffic to the internetgateway.  
1 Private route tables associated with the 3 subnets in AZ1. Configuration of the routes is dependent on the VPC.  
1 Private route tables associated with the 3 subnets in AZ2. Configuration of the routes is dependent on the VPC.  
  
### 2 Network ACL's   
1 Network ACL associated with both Public subnets  
1 Network ACL associated with all 6 Private subnets  
  
Also creates and configures route tables, internet gateway, network ACL's and Virtual Private Gateway for use with a VPN.  
  
___
  
## 2AZNAT.template
  
Deploys 2EIPs, 2 NAT instances,security group,IAM role and adds routes to route tables in an existing VPC (1 per AZ).  
The use of EIP allows the IP's to remain constant even if the instance is retired. Once deployed the private route tables are updated to make the NAT instance the default route for each subnet that it is associated with.  

NAT sizing:
There is a great article discussing NAT instance performance [here:](http://www.azavea.com/blogs/labs/2015/01/selecting-a-nat-instance-size-on-ec2/)