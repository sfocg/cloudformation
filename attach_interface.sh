#!/bin/bash
function log () {
  echo "$(date +"%b %e %T") $@"
  logger -- $(basename $0)" - $@"
}

instance_id=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)
eni_id=$(cat /var/tmp/eni-id.txt)
region=$(curl http://169.254.169.254/latest/dynamic/instance-identity/document|grep region|awk -F\" '{print $4}')

/opt/aws/bin/ec2-attach-network-interface ${eni_id} --instance ${instance_id} --device-index 1 --region ${region}
retcode=$?

[ "$retcode" -eq 0 ] && { log "eni attachment successful" ; exit 0 ; } || { log "eni attachment failed" ; exit 1 ; }